* The [src](src) directory gathers the files needed for generating the pages.
* The [www](www) directory contains the actual pages displayed on the website.


The actual content of the website is the master branch. You should make a merge
request if you want to modify it.