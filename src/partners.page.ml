open Tyxml.Html
open Generate
open Menu

let logo ~background ~name ~file =
      img
      ~src:(Printf.sprintf "images/logo/%s" file)
      ~alt:name
      ~a:[a_style (Printf.sprintf "margin: 5px; \
                                   margin-right: 20px; \
                                   background:%s" background)]
      ()


let () = Generate.page partners  [
    logo
      ~file:"cea_list.png"
      ~name:"CEA List"
      ~background:"#ED1C24";
    logo
      ~file:"upsud.svg"
      ~name:"Paris-Sud University"
      ~background:"white";
    logo
      ~file:"INRIA.png"
      ~name:"INRIA"
      ~background:"white";
    logo
      ~file:"ocamlpro.png"
      ~name:"OcamlPro"
      ~background:"#1B325F";
    logo
      ~file:"adacore.png"
      ~name:"Adacore"
      ~background:"black";
]
