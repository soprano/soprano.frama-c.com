open Tyxml.Html
open Generate
open Menu

let () = Generate.page private_  [
    md "
[[downloads/soprano_propo.pdf|SOPRANO proposition]] et [[downloads/soprano_RetourCoordinateur.pdf|retour coordinateur]]

# Delivrables
  * D1.1 ([[downloads/D1_1.pdf|pdf]]): Requirement Analysis
  * D1.2 ([[downloads/D1_2.pdf|pdf]], [[downloads/D1_2.tar.gz|tar.gz]]): Set of Benchmarks
  * D1.3 ([[downloads/D1_3.pdf|pdf]], [[downloads/D1_3.tar.gz|tar.gz]]): Benchmark Environment (OCI)
  * D4.1 ([[downloads/Deliverable-1.4-FPA-in-Alt-Ergo.tar.gz|tar.gz]]): Floating Point Arithmetic in Alt-Ergo
  * D4.2 ([[downloads/D4_2.pdf|pdf]], [[downloads/D4_2.tar.gz|tar.gz]]): Preliminary Implementation of the SOPRANO-solver (Popop)
  * D2.1 and D2.2 ([[downloads/D2_1-D2_2.pdf|pdf]]) : Combination frameworks
  * D3.1 ([[downloads/D3_1.pdf|pdf]]): Floating Point Arithmetic Solver

# Softwares
  * Alt-Ergo with floating point (tar)
  * Colibri ([[downloads/colibri.tar.gz|tar]])
  * Popop (tar)
  * OCI ([[http://github.com/bobot/OCI/|github]])
  * Ocplib-simplex ([[https://github.com/OCamlPro-Iguernlala/ocplib-simplex/|github]])

# ANR
  * Formulaire du compte-rendu intermédiaire ([[downloads/Formulaire_Compte-rendu_intermediaire.pdf|pdf]], [[downloads/Formulaire_Compte-rendu_intermediaire.docx|docx]])
  * Accord de consortium ([[downloads/Accord_de_consortium_X22760.pdf|pdf]])
  * Présentation revue mi-parcours ([[downloads/soprano_miparcours.pdf|pdf]])

# Meetings
  * 14/01/2015: Kick-off ([[downloads/kickoff_intro.pdf|Introduction]], [[downloads/gatel_soprano.pdf|COLIBRI]], [[downloads/kickoff_popop.pdf|Popop (SOPRANO Solver)]])
  * [[downloads/schedule_150605.txt|05/06/2015]]: Floating Point Arithmetic ([[downloads/colibri_delta.pdf|Delta in COLIBRI]])
  * 08/06/2015: Non-linear arithmetic
  * 07/10/2015: Discussions about FPA in Alt-Ergo
  * [[downloads/schedule_160108.txt|08/01/2016]]: [[downloads/OCI.pdf|OCI]]; [[downloads/real_fp_behavior.pdf|Improvements of FPA on hard problems]]; [[downloads/bitvector_jan8.odp|Bitvectors in Colibri]]
  * [[downloads/schedule_160701.txt|01/07/2016]]: Improvement of FPA in Alt-Ergo; Lesson on the tentative of COLIBRI at SMTCOMP; States of Popop

# Communications
  * Bit-vectors and Integer domains for SMT solving ([[downloads/bv_cpmeetverif_abstract.pdf|abstract]],[[downloads/CPmeetsVerifV3.odp|slides]]), Zakaria Chihani, CP meets Verification 2016 Workshop
  * OCI : For all you Continuous Integration and Benchmarking needs ([[downloads/OCI.pdf|slides]]) , François Bobot, OCaml User in PariS (OUPS)
"
]

(*  LocalWords:  mi-parcours
 *)
