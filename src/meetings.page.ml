open Tyxml.Html
open Generate
open Menu

let () = Generate.page meetings  [
    md "
  * 14/01/2015: Kick-off ([[downloads/kickoff_intro.pdf|Introduction]], [[downloads/gatel_soprano.pdf|COLIBRI]], [[downloads/kickoff_popop.pdf|Popop (SOPRANO Solver)]])
  * 05/06/2015: Floating Point Arithmetic ([[downloads/colibri_delta.pdf|Delta in COLIBRI]])
  * 08/06/2015: Non-linear arithmetic
  * 07/10/2015: Discussions about FPA in Alt-Ergo
  * 08/01/2016: [[downloads/OCI.pdf|OCI]]; [[downloads/real_fp_behavior.pdf|Improvements of FPA on hard problems]]; [[downloads/bitvector_jan8.odp|Bitvectors in Colibri]]
  * 01/07/2016: Improvement of FPA in Alt-Ergo; Lesson on the tentative of COLIBRI at SMTCOMP; States of Popop
"
]
