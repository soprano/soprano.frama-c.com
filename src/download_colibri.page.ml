open Tyxml.Html
open Generate
open Menu

let () = Generate.page download_colibri  [ md_file "colibri_license.md";
                                           p [];
                                           p [pcdata "Download: "; a ~a:([a_href  "downloads/colibri_2176.tar.gz"]) [pcdata "I accept the license"]]]
