open Tyxml.Html
open Generate
open Menu

let () = Generate.page publications  [
    ul [
      li [pcdata "CP meets SMT, François Bobot, Sébastien Bardin, \
                  and Bruno Marre. \
                  Workshop CP meets Verification 2014 (CPCAV 2014)"];
      li [pcdata "Tight coupling between bit-vector and integer \
                  domains can surpass bit-blasting SMT solvers,Zakaria Chihani . \
                  Workshop CP meets Verification 2016"];
    ]
  ]
