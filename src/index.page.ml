open Tyxml.Html
open Generate
open Menu

let p_pcdata txt = p [pcdata txt]

let () = Generate.page main  [
      img
        ~src:"images/general.svg"
        ~alt:"General context"
        ~a:[a_style (Printf.sprintf "margin: 20px; \
                                     float: right;")]
        ();
      md "
The Soprano project is funded by the [[http://www.agence-nationale-recherche.fr/en/\
project-based-funding-to-advance-french-research/|The French National Research Agency]] (ANR). Its main objective is
to improve the reasonning power of current software verification tools in order
to make them more powerful, more efficient and easier to use.

During this project, a new framework for the
cooperation of solvers we will be designed. It will be focused on model
generation and borrowing
principles from SMT (current standard) and CP (well-known in
optimization).
Our main scientific and technical objectives are the following.
The first objective is to design a new collaboration framework for
solvers, centered around synthesis rather than satisfiability and
allowing cooperation beyond that of Nelson-Oppen while still
providing minimal interfaces with theoretical guarantees. The
second objective is to design new decision procedures for
industry-relevant and hard-to-solve theories. The third objective
is to implement these results in a new open-source platform. The
fourth objective is to ensure industrial-adequacy of the techniques
and tools developed through periodical evaluations
from the industrial partners.
"
]
