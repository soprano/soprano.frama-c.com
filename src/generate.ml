open Tyxml.Html
open Menu

let internal_link ?(a=[]) p text =
  Tyxml.Html.a ~a:(a_href (p.filename^".html")::a_title p.long_title::a)
    [pcdata text]

let page p b =
  let header =
    div ~a:[a_id "Header"]
      [internal_link ~a:[a_style "font-size: 200%; margin-right:10px;"]
         main "SOPRANO";
       pcdata (p.long_title)] in
  let menu =
    (List.fold_right (fun p acc -> internal_link p p.menu_title::br ()::acc))
      menu []
  in
  let menu = div ~a:[a_id "Menu"] menu in
  let content = div ~a:[a_id "Content"] [div ~a:[a_class ["readable_div"]] b] in
  let html =
    html
      (head (title (pcdata ("SOPRANO: "^p.long_title)))
         [
           meta ~a:[a_charset "utf-8"] ();
           link ~rel:[`Stylesheet] ~href:"css/main.css" ();
           link ~rel:[`Stylesheet] ~href:"css/lambdoc.css" ();
         ])
      (body ([header;content;menu])) in
  Tyxml.Html.pp () Format.std_formatter html

(** {2 Markdown} *)
module Lambdoc_writer = Lambdoc_whtml_writer.Make (struct
    include Tyxml.Html
    module Svg = Tyxml.Svg
  end)

let md s : [> Html_types.div ] Tyxml.Html.elt =
  let doc = Lambdoc_rlamblite_reader.Trivial.ambivalent_from_string
      ~options:`Markdown s in
  let xdoc = Lambdoc_writer.write_ambivalent doc in
  ( xdoc :
      (Html_types.div Tyxml.Html.elt) :> [> Html_types.div ] Tyxml.Html.elt)

let read_file file =
  let rec aux buf cin =
    match Buffer.add_channel buf cin 128 with
    | () -> aux buf cin
    | exception End_of_file -> Buffer.contents buf
  in
  let cin = open_in file in
  let r  = aux (Buffer.create 1024) cin in
  close_in cin;
  r

let md_file filename : [> Html_types.div ] Tyxml.Html.elt =
  md (read_file filename)

(** {2 For the squeleton } *)
type a_remplir = ARemplir
