open Tyxml.Html
open Generate
open Menu

let () = Generate.page software  [
    md "
  * [[https://github.com/OCamlPro/alt-ergo|Alt-Ergo]]
  * [[download_colibri.html|COLIBRI]]
  * Popop (Soon available as Open-Source)
  * OCI ([[http://github.com/bobot/OCI/|github]])
  * Ocplib-simplex ([[https://github.com/OCamlPro-Iguernlala/ocplib-simplex/|github]])
"
]
