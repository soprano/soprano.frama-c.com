open Tyxml.Html
open Generate
open Menu

let () = Generate.page delivrables  [
    md "
  * D1.1 ([[downloads/D1_1.pdf|pdf]]): Requirement Analysis
  * D1.2 ([[downloads/D1_2.pdf|pdf]], [[downloads/D1_2.tar.gz|tar.gz]]): Set of Benchmarks
  * D1.3 ([[downloads/D1_3.pdf|pdf]], [[downloads/D1_3.tar.gz|tar.gz]]): Benchmark Environment (OCI)
  * D4.1 ([[downloads/Deliverable-1.4-FPA-in-Alt-Ergo.tar.gz|tar.gz]]): Floating Point Arithmetic in Alt-Ergo
  * D4.2 ([[downloads/D4_2.pdf|pdf]], [[downloads/D4_2.tar.gz|tar.gz]]): Preliminary Implementation of the SOPRANO-solver (Popop)
  * D2.1 and D2.2 ([[downloads/D2_1-D2_2.pdf|pdf]]) : Combination frameworks
  * D3.1 ([[downloads/D3_1.pdf|pdf]]): Floating Point Arithmetic Solver
"
]
