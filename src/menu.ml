open Tyxml.Html

type page = {
  long_title: string;
  menu_title: string;
  filename: string; (** If this field is foo the page must
                        be defined in "foo.page.ml" *)
}

let main = {
  long_title = "Home";
  menu_title = "Home";
  filename = "index";
}

let partners = {
  long_title = "Members of the project";
  menu_title = "Partners";
  filename = "partners";
}

let jobs = {
  long_title = "Job opportunities";
  menu_title = "Jobs";
  filename = "jobs";
}

let delivrables = {
  long_title = "Delivrables for the ANR";
  menu_title = "Delivrables";
  filename = "delivrables";
}

let meetings = {
  long_title = "Meetings";
  menu_title = "Meetings";
  filename = "meetings";
}

let publications = {
  long_title = "Publications";
  menu_title = "Publications";
  filename = "publications";
}

let software = {
  long_title = "Software";
  menu_title = "Software";
  filename = "software";
}

let private_ = {
  long_title = "Private";
  menu_title = "Private";
  filename = "private_";
}

let download_colibri = {
  long_title = "Freeware License for COLIBRI and COLIBRI download";
  menu_title = "COLIBRI download";
  filename = "colibri_download";
}

let menu = [
  main;
  partners;
  jobs;
  delivrables;
  meetings;
  publications;
  software;
  private_;
]
